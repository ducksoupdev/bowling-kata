(function() {
	module.exports = function() {
		var bowls = [];
		var currentBowl = 0;

		for (var i = 0; i < 21; i++) {
			bowls[i] = 0;
		}

		function isStrike(frameIndex) {
			return bowls[frameIndex] === "X";
		}

		function isSpare(frameIndex) {
			return bowls[frameIndex] === "/";
		}

		function strikeBonus(frameIndex) {
			var n1 = bowls[frameIndex + 1];
			var n2 = bowls[frameIndex + 2];
			var v1 = 0;
			var v2 = 0;

			if (n1 === "X") {
				v1 = 10;
			} else {
				v1 = parseInt(n1, 10);
			}

			if (n2 === "X") {
				v2 = 10;
			} else if (n2 === "/") {
				v1 = 0;
				v2 = 10; 
			} else {
				v2 = parseInt(n2, 10);
			}

			return v1 + v2;
		}

		function spareBonus(frameIndex) {
			if (bowls[frameIndex + 1] === "X") {
				return 10;
			} else {
				return parseInt(bowls[frameIndex + 1], 10);
			}
		}

		function sumOfPinsInFrame(frameIndex) {
			if (bowls[frameIndex + 1] === "/") {
				if (bowls[frameIndex + 2] === "X") {
					return 20;
				} else {
					return 10 + parseInt(bowls[frameIndex + 2], 10);	
				}
			}
			return parseInt(bowls[frameIndex], 10) + parseInt(bowls[frameIndex + 1], 10);
		}

		return {
			bowl: function(pins) {
				bowls[currentBowl++] = pins;
			},
			score: function() {
				var score = 0;
				var frameIndex = 0;
				for (var frame = 0; frame < 10; frame++) {
					if (isStrike(frameIndex)) {
						score += 10 + strikeBonus(frameIndex);
						frameIndex++;
					} else if (isSpare(frameIndex)) {
						score += 10 + spareBonus(frameIndex);
						frameIndex += 2;
					} else {
						score += sumOfPinsInFrame(frameIndex);
						frameIndex += 2;
					}
				}
				return score;
			}
		}
	}
})();
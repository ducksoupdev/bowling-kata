(function() {
	var Game = require("../game");

	describe("Bowling game", function() {
		var game;

		function bowlNumber(n, pins) {
			for (var i = 0; i < n; i++) {
				game.bowl(pins);
			}
		}

		beforeEach(function() {
			game = new Game();
		});

		it("Should create a new instance of the bowling game", function() {
			expect(game).toBeDefined();
		});

		describe("Test gutter game", function() {
			beforeEach(function() {
				bowlNumber(20, "0");
			});
			
			it("Should add the correct score", function() {
				expect(game.score()).toBe(0);
			});
		});

		describe("Test all same numbers", function() {
			beforeEach(function() {
				bowlNumber(20, "2");
			});
			
			it("Should add the correct score", function() {
				expect(game.score()).toBe(40);
			});
		});

		describe("Test one spare", function() {
			beforeEach(function() {
				bowlNumber(10, "2");
				game.bowl("5");
				game.bowl("/");
				bowlNumber(8, "2");
			});
			
			it("Should add the correct score", function() {
				expect(game.score()).toBe(48);
			});
		});

		describe("Test one strike", function() {
			beforeEach(function() {
				bowlNumber(10, "2");
				game.bowl("X");
				bowlNumber(8, "2");
			});
			
			it("Should add the correct score", function() {
				expect(game.score()).toBe(50);
			});
		});

		describe("Test mixed score", function() {
			beforeEach(function() {
				bowlNumber(4, "2"); // 8
				game.bowl("X"); // 28
				game.bowl("5"); // 
				game.bowl("/"); // 40
				bowlNumber(8, "2"); // 56
				game.bowl("X"); // 76
				game.bowl("5"); // 
				game.bowl("/"); // 86
			});
			
			it("Should add the correct score", function() {
				expect(game.score()).toBe(86);
			});
		});

		describe("Test perfect game", function() {
			beforeEach(function() {
				bowlNumber(20, "X");
			});
			
			it("Should add the correct score", function() {
				expect(game.score()).toBe(300);
			});
		});
	});
})();